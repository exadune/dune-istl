#install headers
install(FILES
   cudavector.hh
   hostvector.hh
   DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/istl/vector)
